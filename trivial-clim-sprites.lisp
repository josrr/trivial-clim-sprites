;;;; trivial-clim-sprites.lisp

(in-package #:trivial-clim-sprites)

(defclass sprite ()
  ((name :initform nil :initarg :name :accessor sprite-name)
   (action :initform nil :initarg :action)
   (length :initform nil :initarg :length :accessor sprite-length)
   (frames :initform 1 :initarg :frames-to-advance :accessor sprite-frames-to-advance)
   (counter :initform 0 :accessor sprite-counter)
   (position :initform 0 :accessor sprite-position)
   (buffer :initform nil :initarg :buffer :accessor sprite-buffer)))

;;(defmethod :after sprite-action ((sprite)))
(defgeneric sprite-action (sprite)
  (:method ((sprite sprite))
    (slot-value sprite 'action)))

(defgeneric (setf sprite-action) (action sprite)
  (:method (action (sprite sprite))
    (setf (slot-value sprite 'position) 0)
    (setf (slot-value sprite 'length) (length (getf (gethash (slot-value sprite 'name) *sprites*)
                                                    action))
          (slot-value sprite 'action) action)))

(defun sprite-width (sprite)
  (let ((textures (getf (gethash (sprite-name sprite) *sprites*)
                        (sprite-action sprite))))
    (when textures
      (pattern-width (car (aref textures 0))))))

(defun sprite-height (sprite)
  (let ((textures (getf (gethash (sprite-name sprite) *sprites*)
                        (sprite-action sprite))))
    (when textures
      (pattern-height (car (aref textures 0))))))

(defun make-sprite (sprite-name sprite-action &key (frames-to-advance 1))
  (let ((textures (getf (gethash sprite-name *sprites*) sprite-action)))
    (if textures
        (make-instance 'sprite
                       :name sprite-name
                       :action sprite-action
                       :length (length textures)
                       :frames-to-advance (or frames-to-advance 1)
                       :buffer (mcclim-render:make-image (pattern-width (car (aref textures 0)))
                                                         (pattern-height (car (aref textures 0)))))
        (error "No such texture (name=~S; action=~S)" sprite-name sprite-action))))

(defun draw-pattern-in-array (pixels pattern x y)
  (declare (optimize (speed 3) (safety 1) (debug 1))
           (type (simple-array (unsigned-byte 32) *) pixels)
           (type clime:image-pattern pattern)
           (type fixnum x y))
  (loop with ancho fixnum = (pattern-width pattern) and alto fixnum = (pattern-height pattern)
        and pattern-pixels of-type (simple-array (unsigned-byte 32) *) = (clime:pattern-array pattern)
        for j fixnum from 0 below alto do
          (loop for i fixnum from 0 below ancho
                for color of-type (unsigned-byte 32) = (aref pattern-pixels j i)
                if (/= color 0)
                  do (setf (aref pixels (the fixnum (+ y j)) (the fixnum (+ x i)))
                           color))))

(defun blend-background (background-image sprite textures x y flip-p)
  (let ((w (sprite-width sprite))
        (h (sprite-height sprite))
        (img (sprite-buffer sprite)))
    (mcclim-render:blend-image background-image x y w h img 0 0)
    (mcclim-render:blend-image (funcall (if flip-p #'cdr #'car)
                                        (aref textures (sprite-position sprite)))
                               0 0 w h img 0 0)
    img))

(defgeneric sprite-draw-frame (sheet sprite x y &optional frame-number background-image flip-p))

(defmethod sprite-draw-frame ((sheet clim:sheet) sprite x y
                              &optional (frame-number 0) background-image flip-p)
  (let ((textures (getf (gethash (sprite-name sprite) *sprites*)
                        (sprite-action sprite))))
    (when textures
      (when (and frame-number (>= frame-number 0) (< frame-number (array-dimension textures 0)))
        (setf (sprite-position sprite) frame-number))
      (draw-pattern* sheet (if background-image
                               (blend-background background-image sprite textures x y flip-p)
                               (funcall (if flip-p #'cdr #'car)
                                        (aref textures (sprite-position sprite))))
                     x y))))

(defmethod sprite-draw-frame ((image clime:image-pattern) sprite x y &optional frame-number background-image flip-p)
  (let ((texturas (getf (gethash (sprite-name sprite) *sprites*)
                        (sprite-action sprite))))
    (when texturas
      (when (and frame-number (>= frame-number 0) (< frame-number (array-dimension texturas 0)))
        (setf (sprite-position sprite) frame-number))
      (when background-image
        (mcclim-render:blend-image background-image x y
                                   (sprite-width sprite) (sprite-height sprite)
                                   image x y))
      (mcclim-render:blend-image (funcall (if flip-p #'cdr #'car)
                                          (aref texturas (sprite-position sprite)))
                                 0 0 (sprite-width sprite) (sprite-height sprite)
                                 image x y))))

(defun sprite-advance-frame (sprite)
  (when (= (sprite-frames-to-advance sprite)
           (incf (sprite-counter sprite)))
    (setf (sprite-position sprite) (if (= (sprite-position sprite)
                                          (1- (sprite-length sprite)))
                                       0
                                       (1+ (sprite-position sprite)))
          (sprite-counter sprite) 0)
    t))

(defgeneric sprite-draw-next-frame (sheet sprite x y &optional background-image flip-p))

(defmethod sprite-draw-next-frame ((sheet clim:sheet) sprite x y &optional background-image flip-p)
  (let ((textures (getf (gethash (sprite-name sprite) *sprites*)
                        (sprite-action sprite))))
    (when textures
      (draw-pattern* sheet
                     (if background-image
                         (blend-background background-image sprite textures x y flip-p)
                         (funcall (if flip-p #'cdr #'car) (aref textures (sprite-position sprite))))
                     x y)
      (sprite-advance-frame sprite))))

(defmethod sprite-draw-next-frame ((image clime:image-pattern) sprite x y &optional background-image flip-p)
  (let ((texturas (getf (gethash (sprite-name sprite) *sprites*)
                        (sprite-action sprite))))
    (when texturas
      (when background-image
        (mcclim-render:blend-image background-image x y
                                   (sprite-width sprite) (sprite-height sprite)
                                   image x y))
      (prog1 (mcclim-render:blend-image (funcall (if flip-p #'cdr #'car)
                                                 (aref texturas (sprite-position sprite)))
                                        0 0 (sprite-width sprite) (sprite-height sprite)
                                        image x y)
        (sprite-advance-frame sprite)))))
