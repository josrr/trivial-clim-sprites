;;;; trivial-clim-sprites.asd

(asdf:defsystem #:trivial-clim-sprites
  :description "A sprite class for McCLIM"
  :author "José Miguel Ángel Ronquillo Rivera"
  :license  "GPLv3"
  :version "0.1"
  :serial t
  :depends-on (#:mcclim #:mcclim-raster-image #:cl-ppcre)
  :components ((:file "package")
               (:file "textures")
               (:file "trivial-clim-sprites")))
