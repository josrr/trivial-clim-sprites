;;;; package.lisp

(defpackage #:trivial-clim-sprites
  (:use #:clim
        #:clim-lisp)
  (:export #:sprite
           #:load-textures
           #:make-sprite
           #:sprite-draw-next-frame
           #:sprite-draw-frame
           #:sprite-width
           #:sprite-height
           #:sprite-action
           #:sprite-advance-frame))
