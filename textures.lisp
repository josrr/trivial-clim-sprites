(in-package #:trivial-clim-sprites)


(defparameter *regex-name* (cl-ppcre:create-scanner "^\([a-zA-Z0-9]+\)_+\([a-zA-Z]+\)_+\([0-9]+\).png$"))

(defparameter *sprites* (make-hash-table :test #'equal))

(defun group-textures (path type)
  (let ((sprites))
    (dolist (file (directory (merge-pathnames type path)))
      (let ((file-name (third (multiple-value-list (uiop/pathname:split-unix-namestring-directory-components (namestring file))))))
        (multiple-value-bind (match parts) (cl-ppcre:scan-to-strings *regex-name* file-name)
          (when match
            (let ((sprite-name (intern (string-upcase (aref parts 0)) 'keyword))
                  (action-name (intern (string-upcase (aref parts 1)) 'keyword)))
              (if (getf sprites sprite-name)
                  (setf (getf (getf sprites sprite-name) action-name)
                        (append (getf (getf sprites sprite-name) action-name)
                                (list file)))
                  (setf (getf sprites sprite-name)
                        (list action-name (list file)))))))))
    sprites))

(defun flip-image (image)
  (let* ((w (pattern-width image))
         (h (pattern-height image))
         (flipped (mcclim-render:make-image w h)))
    (dotimes (j h)
      (dotimes (i w)
        (setf (aref (clime:pattern-array flipped) j i)
              (aref (clime:pattern-array image) j (- (1- w) i)))))
    flipped))

(defun load-textures (&optional (path #P"./pics/") (type "*.png"))
  (labels ((create-array (textures)
             (make-array (length textures)
                         :element-type 'cons
                         :initial-contents (mapcar (lambda (file)
                                                     (let ((img (make-pattern-from-bitmap-file file)))
                                                       (cons img (flip-image img))))
                                                   textures))))
    (loop for (name actions) on (group-textures (or path #P"./pics/") (or type "*.png")) by #'cddr
          do (loop for (action-name textures) on actions by #'cddr
                   if (null (gethash name *sprites*)) do
                     (setf (gethash name *sprites*)
                           (list action-name (create-array textures)))
                   else do
                   (setf (getf (gethash name *sprites*) action-name)
                         (create-array textures))))))
